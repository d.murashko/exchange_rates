### **"Exchange rates"**

This is an application of exchange rates and currency converter.
On each 5th api request was imitated server error.

**`What was used`**

- React.JS
- State management: Redux
- CSS
- Ui framework: Material/io
- Unit tests: jest

**`See application`**

You have to clone this project to your computer and type `npm i` after type command `npm start`

###### or

Click this page: https://di-m-exchange-rates.netlify.app/
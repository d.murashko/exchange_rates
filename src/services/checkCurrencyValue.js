const checkCurrencyValue = (general, val) => {
  if (general * 1.1 >= val && general * 0.9 <= val) {
    return true;
  }
  return false;
};

export default checkCurrencyValue;

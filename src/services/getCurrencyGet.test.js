import getCurrencyGet from "./getCurrencyGet";

const testArray = [
  {
    ccy: "USD",
    base_ccy: "UAH",
    buy: "2",
    sale: "4"
  },
  {
    ccy: "EUR",
    base_ccy: "UAH",
    buy: "4",
    sale: "8"
  },
  {
    ccy: "BTC",
    base_ccy: "USD",
    buy: "100",
    sale: "200"
  }
];

test("The same currency", () => {
  expect(getCurrencyGet("USD", "USD", testArray, 10)).toBe(10);
});

test("Currencies includes in `testArray`", () => {
  expect(getCurrencyGet("EUR", "UAH", testArray, 10)).toBe(40);
});

test("Reverce currencies includes in `testArray`", () => {
  expect(getCurrencyGet("UAH", "USD", testArray, 40)).toBe(10);
});

test("Currency cross rate with same `base_ccy`", () => {
  expect(getCurrencyGet("USD", "EUR", testArray, 20)).toBe(5);
});

test("Currency cross rate across two different `base_ccy`", () => {
  expect(getCurrencyGet("BTC", "EUR", testArray, 10)).toBe(250);
});

test("Currency cross rate with same `base_ccy` from first argument and `ccy` from second one", () => {
  expect(getCurrencyGet("BTC", "UAH", testArray, 10)).toBe(2000);
});

test("Currency cross rate with same `ccy` from first argument and `base_ccy` from second one", () => {
  expect(getCurrencyGet("UAH", "BTC", testArray, 4000)).toBe(5);
});

import checkCurrencyValue from "./checkCurrencyValue";

test("user value not more 10%", () => {
  expect(checkCurrencyValue(25, 27)).toBe(true);
});

test("user value more than 10%", () => {
  expect(checkCurrencyValue(25, 30)).toBe(false);
});

const getCurrencyGet = (from, to, array, val) => {
  // The same currency
  if (from === to) {
    return val;
  }

  // Have exchange rates in table
  let helpObj = array.find(
    (item) =>
      (item.ccy === from && item.base_ccy === to) ||
      (item.ccy === to && item.base_ccy === from)
  );

  if (helpObj) {
    if (helpObj.ccy === from) {
      return +val * +helpObj.buy;
    } else {
      return +val / +helpObj.sale;
    }
  } else {
    // Currency cross rate
    let objWithFrom = array.find((item) => item.ccy === from);
    let objWithTo = array.find((item) => item.ccy === to);

    if (
      objWithFrom &&
      objWithTo &&
      objWithFrom["base_ccy"] === objWithTo["base_ccy"]
    ) {
      return (+val * +objWithFrom.buy) / objWithTo.sale;
    } else if (objWithFrom && objWithTo) {
      // Difficult Currency cross rate
      let arrWithBase = array.find(
        (item) => item.ccy === objWithFrom["base_ccy"]
      );

      if (arrWithBase && arrWithBase["base_ccy"] === objWithTo["base_ccy"]) {
        return (+val * +objWithFrom.buy * arrWithBase.buy) / objWithTo.sale;
      }
    } else if (objWithFrom) {
      objWithTo = array.find(
        (item) => item.base_ccy === to && item.ccy === objWithFrom["base_ccy"]
      );

      if (objWithTo && objWithFrom["base_ccy"] === objWithTo["ccy"]) {
        return +val * +objWithFrom.buy * objWithTo.buy;
      }
    } else if (objWithTo) {
      objWithFrom = array.find(
        (item) => item.base_ccy === from && item.ccy === objWithTo["base_ccy"]
      );

      if (objWithFrom && objWithTo["base_ccy"] === objWithFrom["ccy"]) {
        return +val / +objWithFrom.sale / objWithTo.sale;
      }
    }
  }
};

export default getCurrencyGet;

import LocalStorageEvents from "./LocalStorageEvents";

const checkCounter = (key) => {
  let counterNum = LocalStorageEvents.getData(key);
  if (counterNum < 4 && counterNum) {
    return true;
  }
  return false;
};

export default checkCounter;

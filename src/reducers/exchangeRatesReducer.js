import * as types from "../constants/constTypes";

const initState = {
  exchageRates: []
};

const exchangeRatesReducer = (state = initState, action) => {
  switch (action.type) {
    case types.ADD_EXCHANGE_RATES:
      return {
        ...state,
        ...{ exchageRates: action.payload }
      };

    default:
      return state;
  }
};

export default exchangeRatesReducer;

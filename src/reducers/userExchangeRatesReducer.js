import * as types from "../constants/constTypes";

const initState = {
  userExchageRates: []
};

const userExchangeRatesReducer = (state = initState, action) => {
  switch (action.type) {
    case types.ADD_USER_EXCHANGE_RATES:
      return {
        ...state,
        ...{ userExchageRates: action.payload }
      };

    case types.PUT_USER_EXCHANGE_RATES:
      return {
        ...{ userExchageRates: action.payload }
      };

    default:
      return state;
  }
};

export default userExchangeRatesReducer;

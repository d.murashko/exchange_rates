import { combineReducers } from "redux";
import exchangeRatesReducer from "./exchangeRatesReducer";
import userExchangeRatesReducer from "./userExchangeRatesReducer";

export default combineReducers({
  exchangeRatesReducer,
  userExchangeRatesReducer
});

import React from "react";
import { useDispatch } from "react-redux";
import propTypes from "prop-types";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import InputTableCell from "../../DataEntry/InputTableCell/InputTableCell";
import * as userExchRates from "../../../actions/userExchangeRatesActions";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white
  },
  body: {
    fontSize: 14,
    textAlign: "center"
  },
  root: {
    textAlign: "center",
    borderRight: "1px solid rgba(224, 224, 224, 1)"
  }
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover
    }
  }
}))(TableRow);

const useStyles = makeStyles({
  table: {
    maxWidth: 800,
    margin: "0 auto 50px auto"
  }
});

const TableExchangeRates = (props) => {
  const { listExchRates } = props;
  const classes = useStyles();
  const dispatch = useDispatch();

  const upadeteUserStore = (data) => {
    let newUserList = listExchRates.map((item) => {
      if (item.ccy === data.ccy) {
        return data;
      }
      return item;
    });
    dispatch(userExchRates.updateList(newUserList));
  };

  return (
    <TableContainer className={classes.table} component={Paper}>
      <Table aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>Currensy / Current Date</StyledTableCell>
            <StyledTableCell align="right">Buy</StyledTableCell>
            <StyledTableCell align="right">Sell</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {listExchRates.map((row, i) => (
            <StyledTableRow key={i}>
              <StyledTableCell component="th" scope="row">
                {row.ccy}/{row.base_ccy}
              </StyledTableCell>
              <StyledTableCell align="right">
                <InputTableCell
                  value={row.buy}
                  operation={"buy"}
                  data={row}
                  updateStore={upadeteUserStore}
                />
              </StyledTableCell>
              <StyledTableCell align="right">
                <InputTableCell
                  value={row.sale}
                  operation={"sale"}
                  data={row}
                  updateStore={upadeteUserStore}
                />
              </StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

TableExchangeRates.propTypes = {
  listExchRates: propTypes.array
};

export default TableExchangeRates;

import React from "react";
import "./Header.css";
import propTypes from "prop-types";

const Header = (props) => {
  const { logo } = props;
  return (
    <header className="header">
      <div className="logo">
        <a className="logo-text" href="/">
          {logo}
        </a>
      </div>
      <div className="logo-line"></div>
    </header>
  );
};

Header.propTypes = {
  logo: propTypes.string
};

export default Header;

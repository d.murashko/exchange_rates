import React from "react";
import { useSelector } from "react-redux";
import "./Content.css";
import TableExchangeRates from "../../DataDisplay/TableExchangeRates.js/TableExchangeRates";
import CurrencyConverter from "../../DataEntry/CurrencyConverter/CurrencyConverter";

const Content = () => {
  const listExchRates = useSelector(
    (state) => state.exchangeRatesReducer.exchageRates
  );
  const userExchRates = useSelector(
    (state) => state.userExchangeRatesReducer.userExchageRates
  );

  return (
    <main className="content">
      <TableExchangeRates listExchRates={listExchRates} />
      <CurrencyConverter userExchRates={userExchRates} />
    </main>
  );
};

export default Content;

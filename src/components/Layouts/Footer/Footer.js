import React from "react";
import "./Footer.css";
import propTypes from "prop-types";

const Footer = (props) => {
  const { copyright } = props;
  return (
    <footer className="footer">
      <p className="copyright">{copyright}</p>
    </footer>
  );
};

Footer.propTypes = {
  copyright: propTypes.string
};

export default Footer;

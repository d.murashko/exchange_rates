import React, { useState } from "react";
import propTypes from "prop-types";
import "./InputTableCell.css";
import { makeStyles } from "@material-ui/core/styles";
import CreateIcon from "@material-ui/icons/Create";
import IconButton from "@material-ui/core/IconButton";
import CheckIcon from "@material-ui/icons/Check";
import CloseIcon from "@material-ui/icons/Close";
import checkCurrencyValue from "../../../services/checkCurrencyValue";
import TextField from "@material-ui/core/TextField";

const useStyles = makeStyles({
  inputCell: {
    position: "relative",

    "& .MuiInput-input": {
      textAlign: "center",
      color: "#000"
    },
    "& .MuiInput-underline::before": {
      border: "none"
    },
    "& .MuiFormHelperText-root": {
      position: "absolute",
      bottom: -18,
      left: 0
    }
  },
  edit: {
    position: "absolute",
    top: -10,
    right: -10,
    width: "20px",
    height: "20px"
  },
  blockAccept: {
    position: "absolute",
    top: -15,
    right: -15,
    display: "flex"
  },
  btnAcceptCell: {
    width: "20px",
    height: "20px"
  },
  btnCancelCell: {
    width: "20px",
    height: "20px"
  }
});

const InputTableCell = (props) => {
  const { value, operation, data, updateStore } = props;
  const classes = useStyles();

  const [editCell, setEditCell] = useState(true);
  const [displayIconEdit, setDisplayIconEdit] = useState(false);
  const [prevValue, setPrevValue] = useState(value);
  const [newValue, setNewValue] = useState(value);
  const [errorValue, setErrorValue] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const changeDisabledCell = () => {
    setEditCell(false);
    setDisplayIconEdit(false);
  };

  const showIcon = () => {
    setDisplayIconEdit(true);
  };

  const hideIcon = () => {
    setDisplayIconEdit(false);
  };

  const cancelEdit = () => {
    setEditCell(true);
    setDisplayIconEdit(false);
    setNewValue(prevValue);
    setErrorValue(false);
    setErrorMessage("");
  };

  const acceptEdit = () => {
    setPrevValue(newValue);
    setEditCell(true);
    setDisplayIconEdit(true);
    let userData = { ...data };
    userData[operation] = `${newValue}`;
    updateStore(userData);
  };

  const onChange = (e) => {
    setNewValue(e.target.value);
    if (checkCurrencyValue(+value, +e.target.value)) {
      setErrorValue(false);
      setErrorMessage("");
    } else {
      setErrorValue(true);
      setErrorMessage("current+-10%");
    }
  };

  return (
    <div>
      {editCell && (
        <div
          className={classes.inputCell}
          onMouseOver={showIcon}
          onMouseLeave={hideIcon}
        >
          <TextField
            type="number"
            fullWidth={true}
            value={newValue}
            disabled={editCell}
          />
          {displayIconEdit && (
            <IconButton className={classes.edit} onClick={changeDisabledCell}>
              <CreateIcon />
            </IconButton>
          )}
        </div>
      )}

      {!editCell && (
        <div className={classes.inputCell}>
          <TextField
            error={errorValue}
            name="newValueInput"
            type="number"
            fullWidth={true}
            value={newValue}
            autoFocus={true}
            onChange={onChange}
            helperText={errorMessage}
            // onBlur={cancelEdit}
          />
          <div className={classes.blockAccept}>
            {!errorValue && (
              <IconButton
                className={classes.btnAcceptCell}
                onClick={acceptEdit}
              >
                <CheckIcon />
              </IconButton>
            )}
            <IconButton className={classes.btnCancelCell} onClick={cancelEdit}>
              <CloseIcon />
            </IconButton>
          </div>
        </div>
      )}
    </div>
  );
};

InputTableCell.propTypes = {
  value: propTypes.string,
  operation: propTypes.string,
  data: propTypes.object,
  updateStore: propTypes.func
};

export default InputTableCell;

import React, { useState } from "react";
import propTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import SwapHorizIcon from "@material-ui/icons/SwapHoriz";
import getCurrencyGet from "../../../services/getCurrencyGet";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    justifyContent: "space-around",
    alignItems: "center",
    flexWrap: "wrap",
    maxWidth: 800,
    margin: "0 auto",

    [theme.breakpoints.down(820)]: {
      maxWidth: 420
    },

    "& .MuiTextField-root": {
      margin: theme.spacing(1)
    }
  },
  currency: {
    width: 120
  }
}));

const CurrencyConverter = (props) => {
  const { userExchRates } = props;
  const classes = useStyles();
  const currencyArray = [];

  userExchRates.forEach((item) => {
    currencyArray.push(item.ccy);
    currencyArray.push(item.base_ccy);
  });
  const currChangeNoDuplicat = Array.from(new Set(currencyArray));

  const [currencyChange, setCurrencyChange] = useState("");
  const [currencyGet, setCurrencyGet] = useState("");
  const [valueChange, setValueChange] = useState("");
  const [valueGet, setValueGet] = useState("");

  const handleCurrencyChange = (event) => {
    setCurrencyChange(event.target.value);
    setValueGet("");
  };

  const handleCurrencyGet = (event) => {
    setCurrencyGet(event.target.value);
    setValueGet("");
  };

  const changeCurrencies = () => {
    setCurrencyChange(currencyGet);
    setCurrencyGet(currencyChange);
    setValueGet("");
  };

  const onChangeInputChange = (e) => {
    setValueChange(e.target.value);
  };

  const convert = () => {
    if (currencyGet === "" || currencyChange === "") {
      return false;
    }

    let getValue = getCurrencyGet(
      currencyChange,
      currencyGet,
      userExchRates,
      valueChange
    );
    setValueGet(getValue);
  };

  return (
    <form className={classes.root} noValidate autoComplete="off">
      <div className={classes.root}>
        <TextField
          id="outlined-helperText"
          label="Change currency"
          type="number"
          variant="outlined"
          color="primary"
          value={valueChange}
          onChange={onChangeInputChange}
        />

        <TextField
          className={classes.currency}
          id="outlined-select-currency"
          select
          label="Currency"
          value={currencyChange}
          onChange={handleCurrencyChange}
          variant="outlined"
        >
          {currChangeNoDuplicat.map((option, i) => (
            <MenuItem key={i} value={option}>
              {option}
            </MenuItem>
          ))}
        </TextField>
      </div>

      <div>
        <Button
          aria-label="convert"
          variant="contained"
          onClick={changeCurrencies}
        >
          <SwapHorizIcon />
        </Button>
      </div>

      <div className={classes.root}>
        <TextField
          disabled
          id="outlined-read-only-input"
          label="Get currency"
          type="number"
          InputProps={{
            readOnly: true
          }}
          InputLabelProps={{
            shrink: true
          }}
          variant="outlined"
          color="primary"
          value={valueGet}
        />

        <TextField
          className={classes.currency}
          id="outlined-select-currency"
          select
          label="Currency"
          value={currencyGet}
          onChange={handleCurrencyGet}
          variant="outlined"
        >
          {currChangeNoDuplicat.map((option, i) => (
            <MenuItem key={i} value={option}>
              {option}
            </MenuItem>
          ))}
        </TextField>
      </div>

      <div>
        <Button
          aria-label="convert"
          color="primary"
          variant="contained"
          onClick={convert}
        >
          {"Calculate"}
        </Button>
      </div>
    </form>
  );
};

CurrencyConverter.propTypes = {
  userExchRates: propTypes.array
};

export default CurrencyConverter;

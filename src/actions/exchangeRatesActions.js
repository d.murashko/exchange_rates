import * as types from "../constants/constTypes";

export function getList(list) {
  return (dispatch) => {
    dispatch({
      type: types.ADD_EXCHANGE_RATES,
      payload: list
    });
  };
}

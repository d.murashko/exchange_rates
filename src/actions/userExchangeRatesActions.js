import * as types from "../constants/constTypes";

export function getList(list) {
  return (dispatch) => {
    dispatch({
      type: types.ADD_USER_EXCHANGE_RATES,
      payload: list
    });
  };
}

export function updateList(list) {
  return (dispatch) => {
    dispatch({
      type: types.PUT_USER_EXCHANGE_RATES,
      payload: list
    });
  };
}

import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import "./App.css";
import Header from "../components/Layouts/Header/Header";
import Content from "../components/Layouts/Content/Content";
import Footer from "../components/Layouts/Footer/Footer";
import getExchangeRatesApi from "../services/generalApi";
import * as url from "../constants/constURL";
import * as exchRates from "../actions/exchangeRatesActions";
import * as userExchRates from "../actions/userExchangeRatesActions";
import LocalStorageEvents from "../services/LocalStorageEvents";
import checkCounter from "../services/checkCounter";
import ErrorField from "../components/Layouts/ErrorField/ErrorField";

export default function App() {
  const dispatch = useDispatch();
  const [errorField, setErrorField] = useState(false);

  useEffect(() => {
    getExchangeRatesApi(url.exchangeRatesULR).then((data) => {
      console.log("Resived data from url -->", data);
      let filteredArr = data.filter((item) => item.ccy !== "RUR");
      dispatch(exchRates.getList(filteredArr));
      dispatch(userExchRates.getList(filteredArr));
    });

    let counterNum = LocalStorageEvents.getData("counter");

    if (checkCounter("counter")) {
      LocalStorageEvents.setData("counter", counterNum + 1);
      setErrorField(false);
    } else if (!counterNum) {
      counterNum = 1;
      LocalStorageEvents.setData("counter", counterNum);
      setErrorField(false);
    } else {
      LocalStorageEvents.clearData();
      setErrorField(true);
    }
  }, [dispatch]);

  return (
    <>
      {!errorField && (
        <div className="app">
          <Header logo={"Exchange Rates"} />
          <Content />
          <Footer copyright={"2020 All right reserved"} />
        </div>
      )}
      {errorField && <ErrorField />}
    </>
  );
}
